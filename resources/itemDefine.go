package resources

import (
	"bitbucket.org/directivegames/simple-server/aws/dynamodb"
	"fmt"
	"github.com/smugmug/godynamo/types/item"
	"strconv"
)

type PlayerItem struct {
	ItemID int64
	TypeID int64
	Level  uint
	PosX   int
	PosY   int
	PosZ   int
}

func (this PlayerItem) String() string {
	return fmt.Sprintf("PlayerItem {itemID=%d, typeID=%d, level=%d, pos=(%d, %d, %d)}",
		this.ItemID, this.TypeID, this.Level, this.PosX, this.PosY, this.PosZ)
}

func (this *PlayerItem) FromDBItem(source item.Item) error {
	var err error
	if v, ok := source["ItemID"]; ok {
		this.ItemID, err = strconv.ParseInt(v.N, 10, 64)
		if err != nil {
			return fmt.Errorf("failed to convert itemID: %s", err.Error())
		}
	} else {
		return fmt.Errorf("Missing ItemID from the record")
	}

	if v, ok := source["TypeID"]; ok {
		this.TypeID, err = strconv.ParseInt(v.N, 10, 64)
		if err != nil {
			return fmt.Errorf("failed to convert typeID: %s", err.Error())
		}
	} else {
		return fmt.Errorf("Missing TypeID from the record")
	}

	if v, ok := source["Level"]; ok {
		converted, err := strconv.Atoi(v.N)
		if err != nil {
			return fmt.Errorf("Failed to convert Level field: %s", err.Error())
		} else {
			this.Level = uint(converted)
		}
	} else {
		return fmt.Errorf("Missing Level from the record")
	}

	if v, ok := source["PosX"]; ok {
		converted, err := strconv.Atoi(v.N)
		if err != nil {
			return fmt.Errorf("Failed to convert PosX field: %s", err.Error())
		} else {
			this.PosX = converted
		}
	} else {
		return fmt.Errorf("Missing PosX from the record")
	}

	if v, ok := source["PosY"]; ok {
		converted, err := strconv.Atoi(v.N)
		if err != nil {
			return fmt.Errorf("Failed to convert PosY field: %s", err.Error())
		} else {
			this.PosY = converted
		}
	} else {
		return fmt.Errorf("Missing PosY from the record")
	}

	if v, ok := source["PosZ"]; ok {
		converted, err := strconv.Atoi(v.N)
		if err != nil {
			return fmt.Errorf("Failed to convert PosZ field: %s", err.Error())
		} else {
			this.PosZ = converted
		}
	} else {
		return fmt.Errorf("Missing PosZ from the record")
	}

	return nil
}

func (this *PlayerItem) ToDBItem(playerID int64) item.Item {
	row := item.NewItem()
	row["PlayerID"] = dynamodb.GetNumberAttribute(playerID)
	row["ItemID"] = dynamodb.GetNumberAttribute(this.ItemID)
	row["TypeID"] = dynamodb.GetNumberAttribute(this.TypeID)
	row["Level"] = dynamodb.GetNumberAttribute(int64(this.Level))
	row["PosX"] = dynamodb.GetNumberAttribute(int64(this.PosX))
	row["PosY"] = dynamodb.GetNumberAttribute(int64(this.PosY))
	row["PosZ"] = dynamodb.GetNumberAttribute(int64(this.PosZ))
	return row
}
