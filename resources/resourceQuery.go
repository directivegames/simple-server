package resources

import (
	"bitbucket.org/directivegames/simple-server/aws/dynamodb"
	log "github.com/cihub/seelog"
	"github.com/smugmug/godynamo/types/item"
)

const (
	// resource names
	Gold       = "gold"
	Cement     = "cement"
	Fuel       = "fuel"
	Steel      = "steel"
	ScrapMetal = "scrapMetal"
	RareMetal  = "rareMetal"
)

var (
	ResourceNames = []string{
		Gold,
		Cement,
		Fuel,
		Steel,
		ScrapMetal,
		RareMetal,
	}

	playerResourcesTable = dynamodb.Table{
		TableName:    "PlayerResources",
		KeyName:      "PlayerID",
		RangeKeyName: "ResourceType",
	}
)

func GetStored(playerID int64) (*ResourceCollection, error) {
	stored, _, err := GetStoredAndCapacity(playerID)
	return stored, err
}

func GetCapacity(playerID int64) (*ResourceCollection, error) {
	_, capacity, err := GetStoredAndCapacity(playerID)
	return capacity, err
}

func GetStoredAndCapacity(playerID int64) (*ResourceCollection, *ResourceCollection, error) {
	keys := make([]item.Item, 0)
	for _, name := range ResourceNames {
		item := item.NewItem()
		item[playerResourcesTable.KeyName] = dynamodb.GetNumberAttribute(playerID)
		item[playerResourcesTable.RangeKeyName] = dynamodb.GetStringAttribute(name)
		keys = append(keys, item)
	}

	items, err := playerResourcesTable.BatchGetItems(keys)
	if err != nil {
		return nil, nil, err
	}

	stored := ResourceCollection{}
	capacity := ResourceCollection{}
	for _, elem := range items {
		resName, err := dynamodb.GetStringValue(elem, playerResourcesTable.RangeKeyName, false)
		if err != nil {
			continue
		}
		parsed, err := dynamodb.GetNumberValue(elem, "Stored", true)
		if parsed != nil {
			stored.AddResource(*resName, *parsed, false)
		} else if err != nil {
			log.Errorf("GetStoredAndCapacity: %s", err.Error())
		}

		parsed, err = dynamodb.GetNumberValue(elem, "Capacity", true)
		if parsed != nil {
			capacity.AddResource(*resName, *parsed, false)
		} else if err != nil {
			log.Errorf("GetStoredAndCapacity: %s", err.Error())
		}
	}

	return &stored, &capacity, nil
}
