package resources

import (
	"bitbucket.org/directivegames/simple-server/aws/dynamodb"
	custom "bitbucket.org/directivegames/simple-server/errors"
	"bitbucket.org/directivegames/simple-server/schemas"
	"bitbucket.org/directivegames/simple-server/util"
	"fmt"
	"github.com/smugmug/godynamo/types/item"
	"strconv"
)

type ResourceCollection struct {
	Gold       int64
	Cement     int64
	Fuel       int64
	Steel      int64
	ScrapMetal int64
	RareMetal  int64
}

func CreateFromResourceDetails(details []schemas.ResourceDetail) (*ResourceCollection, error) {
	collection := ResourceCollection{}
	for _, elem := range details {
		err := collection.AddResource(elem.ResourceType, elem.Amount, true)
		if err != nil {
			return nil, err
		}
	}
	return &collection, nil
}

func (this ResourceCollection) String() string {
	return fmt.Sprintf("ResourceCollection {gold=%d, cement=%d, fuel=%d, steel=%d, scrapMetal=%d, rareMetal=%d}",
		this.Gold, this.Cement, this.Fuel, this.Steel, this.ScrapMetal, this.RareMetal)
}

func (this *ResourceCollection) AddResource(resName string, amount int64, checkResName bool) error {
	if amount < 0 {
		return fmt.Errorf("invalid resource amount: %d", amount)
	}
	switch resName {
	case Gold:
		this.Gold += amount
	case Cement:
		this.Cement += amount
	case Fuel:
		this.Fuel += amount
	case Steel:
		this.Steel += amount
	case ScrapMetal:
		this.ScrapMetal += amount
	case RareMetal:
		this.RareMetal += amount
	default:
		if checkResName {
			return fmt.Errorf("invalid resource name: %s", resName)
		}
	}
	return nil
}

func (this *ResourceCollection) GetResource(resName string) int64 {
	switch resName {
	case Gold:
		return this.Gold
	case Cement:
		return this.Cement
	case Fuel:
		return this.Fuel
	case Steel:
		return this.Steel
	case ScrapMetal:
		return this.ScrapMetal
	case RareMetal:
		return this.RareMetal
	default:
		return 0
	}
}

func (this *ResourceCollection) Reset() {
	this.Gold = 0
	this.Cement = 0
	this.Fuel = 0
	this.Steel = 0
	this.ScrapMetal = 0
	this.RareMetal = 0
}

func (this *ResourceCollection) Multiply(factor float32) ResourceCollection {
	return ResourceCollection{
		Gold:       int64(float32(this.Gold) * factor),
		Cement:     int64(float32(this.Cement) * factor),
		Fuel:       int64(float32(this.Fuel) * factor),
		Steel:      int64(float32(this.Steel) * factor),
		ScrapMetal: int64(float32(this.ScrapMetal) * factor),
		RareMetal:  int64(float32(this.RareMetal) * factor),
	}
}

func (this *ResourceCollection) Add(other ResourceCollection) ResourceCollection {
	return ResourceCollection{
		Gold:       this.Gold + other.Gold,
		Cement:     this.Cement + other.Cement,
		Fuel:       this.Fuel + other.Fuel,
		Steel:      this.Steel + other.Steel,
		ScrapMetal: this.ScrapMetal + other.ScrapMetal,
		RareMetal:  this.RareMetal + other.RareMetal,
	}
}

func (this *ResourceCollection) Contains(other ResourceCollection) bool {
	if other.Gold > this.Gold {
		return false
	}

	if other.Cement > this.Cement {
		return false
	}

	if other.Fuel > this.Fuel {
		return false
	}

	if other.Steel > this.Steel {
		return false
	}

	if other.ScrapMetal > this.ScrapMetal {
		return false
	}

	if other.RareMetal > this.RareMetal {
		return false
	}

	return true
}

func (this *ResourceCollection) GetCapped(limit ResourceCollection) ResourceCollection {
	return ResourceCollection{
		Gold:       util.MinInt64(this.Gold, limit.Gold),
		Cement:     util.MinInt64(this.Cement, limit.Cement),
		Fuel:       util.MinInt64(this.Fuel, limit.Fuel),
		Steel:      util.MinInt64(this.Steel, limit.Steel),
		ScrapMetal: util.MinInt64(this.ScrapMetal, limit.ScrapMetal),
		RareMetal:  util.MinInt64(this.RareMetal, limit.RareMetal),
	}
}

func (this *ResourceCollection) CheckResources() error {
	message := ""
	if this.Gold < 0 {
		message += fmt.Sprintf(" gold=%d", this.Gold)
	}

	if this.Cement < 0 {
		message += fmt.Sprintf(" cement=%d", this.Cement)
	}

	if this.Fuel < 0 {
		message += fmt.Sprintf(" fuel=%d", this.Fuel)
	}

	if this.RareMetal < 0 {
		message += fmt.Sprintf(" rareMetal=%d", this.RareMetal)
	}

	if this.ScrapMetal < 0 {
		message += fmt.Sprintf(" scrapMetal=%d", this.ScrapMetal)
	}

	if this.Steel < 0 {
		message += fmt.Sprintf(" steel=%d", this.Steel)
	}

	if len(message) > 0 {
		return fmt.Errorf("invalid balance:%s", message)
	} else {
		return nil
	}
}

func (this *ResourceCollection) FromDBItem(source item.Item) error {
	this.Reset()
	if source != nil {
		for name, value := range source {
			amount, err := strconv.ParseInt(value.N, 10, 64)
			if err != nil {
				return custom.StrParseError{err}
			}
			err = this.AddResource(name, amount, false)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (this *ResourceCollection) ToDBItem() item.Item {
	item := item.NewItem()
	item[Gold] = dynamodb.GetNumberAttribute(this.Gold)
	item[Cement] = dynamodb.GetNumberAttribute(this.Cement)
	item[Fuel] = dynamodb.GetNumberAttribute(this.Fuel)
	item[Steel] = dynamodb.GetNumberAttribute(this.Steel)
	item[ScrapMetal] = dynamodb.GetNumberAttribute(this.ScrapMetal)
	item[RareMetal] = dynamodb.GetNumberAttribute(this.RareMetal)
	return item
}
