package resources

import (
	"bitbucket.org/directivegames/simple-server/id"
	"bitbucket.org/directivegames/simple-server/schemas"
	"bitbucket.org/directivegames/simple-server/static"
	"fmt"
)

func CreateDefaultItems(playerID int64, templates []schemas.BuildingPlacement) error {
	if len(templates) == 0 {
		return nil
	}

	items := make([]PlayerItem, 0)
	for _, elem := range templates {
		itemID, err := id.GenerateNewID("ItemID")
		if err != nil {
			return err
		}
		item := PlayerItem{
			ItemID: itemID,
			TypeID: elem.TypeID,
			Level:  0,
			PosX:   int(elem.Location.X),
			PosY:   int(elem.Location.Y),
			PosZ:   int(elem.Location.Z)}
		items = append(items, item)
	}

	return updateItemsInDB(playerID, items)
}

func CreateItem(playerID int64, template *PlayerItem) (*PlayerItem, error) {
	// currently assumes all the items are buildings...
	itemSpec := new(schemas.BuildingSpec)
	err := static.PopulateType(template.TypeID, itemSpec)
	if err != nil {
		return nil, fmt.Errorf("failed to load item spec: %s", err.Error())
	}

	// cost validation
	if len(itemSpec.Levels) == 0 {
		return nil, fmt.Errorf("item has no levels")
	}

	levelInfo := itemSpec.Levels[0]
	cost, err := CreateFromResourceDetails(levelInfo.Cost)
	if err != nil {
		return nil, fmt.Errorf("error when updating item cost: %s", err.Error())
	}

	balance, err := GetStored(playerID)
	if err != nil {
		return nil, fmt.Errorf("failed to update player balance: %s", err.Error())
	}

	if !balance.Contains(*cost) {
		return nil, fmt.Errorf("insufficient resources")
	}

	// TODO: location validation

	// TODO: check HQ constraint

	template.ItemID, err = id.GenerateNewID("ItemID")
	if err != nil {
		return nil, fmt.Errorf("failed to generate itemID: %s", err.Error())
	}

	err = updateItemsInDB(playerID, []PlayerItem{*template})
	if err != nil {
		return nil, fmt.Errorf("failed to update item in DB: %s", err.Error())
	}

	_, err = ConsumeResources(playerID, *cost)
	if err != nil {
		return nil, fmt.Errorf("failed to consume resources")
	}

	return template, nil
}
