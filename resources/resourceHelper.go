package resources

import (
	"bitbucket.org/directivegames/simple-server/schemas"
	"bitbucket.org/directivegames/simple-server/static"
	"fmt"
)

type resourceHelper struct {
}

func (this *resourceHelper) getItemSpec(typeID int64) (*schemas.BuildingSpec, error) {
	itemSpec := new(schemas.BuildingSpec)
	err := static.PopulateType(typeID, itemSpec)
	if err != nil {
		return nil, err
	} else {
		return itemSpec, nil
	}
}

func (this *resourceHelper) getLevelInfo(typeID int64, level uint) (*schemas.BuildingLevelInfo, error) {
	itemSpec, err := this.getItemSpec(typeID)
	if err != nil {
		return nil, err
	}

	if int(level) >= len(itemSpec.Levels) {
		return nil, fmt.Errorf("invalid level index: %d, item only has %d levels", level, len(itemSpec.Levels))
	} else {
		return &itemSpec.Levels[level], nil
	}
}

func (this *resourceHelper) getCost(typeID int64, level uint) (*ResourceCollection, error) {
	levelInfo, err := this.getLevelInfo(typeID, level)
	if err != nil {
		return nil, err
	}
	return CreateFromResourceDetails(levelInfo.Cost)
}

func (this *resourceHelper) getStorage(typeID int64, level uint) (*ResourceCollection, error) {
	levelInfo, err := this.getLevelInfo(typeID, level)
	if err != nil {
		return nil, err
	}
	return CreateFromResourceDetails(levelInfo.Storage)
}

func (this *resourceHelper) getOutput(typeID int64, level uint) (*ResourceCollection, error) {
	levelInfo, err := this.getLevelInfo(typeID, level)
	if err != nil {
		return nil, err
	}
	return CreateFromResourceDetails(levelInfo.Output)
}
