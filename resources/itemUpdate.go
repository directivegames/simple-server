package resources

import (
	"bitbucket.org/directivegames/simple-server/aws/dynamodb"
	"github.com/smugmug/godynamo/types/attributevalue"
	"github.com/smugmug/godynamo/types/item"
)

func DeleteItemsInDB(playerID int64) error {
	items, err := GetPlayerItems(playerID)
	if err != nil {
		return err
	}

	if len(items) == 0 {
		return nil
	}

	hkeys := make([]*attributevalue.AttributeValue, len(items))
	rkeys := make([]*attributevalue.AttributeValue, len(items))

	for index, elem := range items {
		hkeys[index] = dynamodb.GetNumberAttribute(playerID)
		rkeys[index] = dynamodb.GetNumberAttribute(elem.ItemID)
	}
	err = playerItemsTable.DeleteItemsWithHashAndRangeKey(hkeys, rkeys)
	return err
}

func updateItemsInDB(playerID int64, items []PlayerItem) error {
	rows := make([]item.Item, len(items))
	for index, elem := range items {
		rows[index] = elem.ToDBItem(playerID)
	}
	err := playerItemsTable.UpdateItems(rows)
	if err == nil {
		updateCapacity(playerID)
	}
	return err
}

func UpdateItem(playerID int64, request *PlayerItem) (*PlayerItem, error) {
	panic("not implemented")
	return nil, nil
}
