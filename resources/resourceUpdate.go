package resources

import (
	"bitbucket.org/directivegames/simple-server/aws/dynamodb"
	"fmt"
	log "github.com/cihub/seelog"
	"github.com/smugmug/godynamo/types/item"
)

func updateCapacity(playerID int64) (*ResourceCollection, error) {
	items, err := GetPlayerItems(playerID)
	if err != nil {
		log.Errorf("updateCapacity: %s, player=%d", err.Error(), playerID)
		return nil, err
	}

	capacity := ResourceCollection{}
	helper := resourceHelper{}
	for _, elem := range items {
		single, err := helper.getStorage(elem.TypeID, elem.Level)
		if err != nil {
			log.Errorf("updateCapacity: %s, typeID=%d, level=%d", err.Error(), elem.TypeID, elem.Level)
			return nil, err
		}
		capacity = capacity.Add(*single)
	}

	err = saveCapacity(playerID, capacity)

	if err != nil {
		return nil, err
	} else {
		return &capacity, nil
	}
}

func ConsumeResources(playerID int64, resources ResourceCollection) (*ResourceCollection, error) {
	stored, _, err := GetStoredAndCapacity(playerID)

	if err != nil {
		return nil, err
	}

	if !stored.Contains(resources) {
		return nil, fmt.Errorf("insufficient balance")
	}

	newStored := stored.Add(resources.Multiply(-1))
	err = saveStored(playerID, newStored)
	if err != nil {
		return nil, err
	}

	return stored, nil
}

func AddResources(playerID int64, resources ResourceCollection) (*ResourceCollection, error) {
	stored, capacity, err := GetStoredAndCapacity(playerID)

	if err != nil {
		return nil, err
	}

	newStored := stored.Add(resources)
	newStored = newStored.GetCapped(*capacity)
	err = saveStored(playerID, newStored)
	if err != nil {
		return nil, err
	}

	return stored, nil
}

func saveStoredAndCapacity(playerID int64, stored ResourceCollection, capcity ResourceCollection) error {
	items := make([]item.Item, 0)
	for _, resName := range ResourceNames {
		item := item.NewItem()
		item["PlayerID"] = dynamodb.GetNumberAttribute(playerID)
		item["ResourceType"] = dynamodb.GetStringAttribute(resName)
		item["Stored"] = dynamodb.GetNumberAttribute(stored.GetResource(resName))
		item["Capacity"] = dynamodb.GetNumberAttribute(capcity.GetResource(resName))
		items = append(items, item)
	}
	err := playerResourcesTable.UpdateItems(items)
	return err
}

func saveStored(playerID int64, stored ResourceCollection) error {
	_, capacity, err := GetStoredAndCapacity(playerID)
	if err != nil {
		return err
	}
	return saveStoredAndCapacity(playerID, stored, *capacity)
}

func saveCapacity(playerID int64, capacity ResourceCollection) error {
	stored, _, err := GetStoredAndCapacity(playerID)
	if err != nil {
		return err
	}
	return saveStoredAndCapacity(playerID, *stored, capacity)
}
