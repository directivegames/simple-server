package resources

import (
	"bitbucket.org/directivegames/simple-server/aws/dynamodb"
	"fmt"
)

var (
	playerItemsTable = dynamodb.Table{
		TableName:    "PlayerItems",
		KeyName:      "PlayerID",
		RangeKeyName: "ItemID",
	}
)

func GetPlayerItems(playerID int64) ([]PlayerItem, error) {
	rows, err := playerItemsTable.QueryWithHashKey(dynamodb.GetNumberAttribute(playerID))
	if err != nil {
		return nil, err
	}

	items := make([]PlayerItem, 0)
	for _, elem := range rows {
		item := PlayerItem{}
		item.FromDBItem(elem)
		items = append(items, item)
	}

	return items, nil
}

func GetPlayerItem(playerID int64, itemID int64) (*PlayerItem, error) {
	rows, err := playerItemsTable.QueryWithHashAndRangeKey(dynamodb.GetNumberAttribute(playerID), dynamodb.GetNumberAttribute(itemID))
	if err != nil {
		return nil, err
	}

	if len(rows) > 0 {
		item := PlayerItem{}
		item.FromDBItem(rows[0])
		return &item, nil
	} else {
		return nil, fmt.Errorf("The requested item doesn't exist")
	}
}
