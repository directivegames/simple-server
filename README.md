Simple HTTP Server
==================
A simple HTTP server implemented in golang for the prototype of "The Machines" project

Dependencies
-----------------
The stack requires the following components to compile and run:

### Golang Runtime
Please install the latest vesion from [https://golang.org/dl/]

### gin
The server is built on top of the gin web framework, which can be installed with **go get github.com/gin-gonic/gin**

### jwt-go
This is the package used for json web token handling, install it with **go get github.com/dgrijalva/jwt-go**

### godynamo
This is the golang driver for the aws dynamodb service, install it with **github.com/smugmug/godynamo**

### beego/cache
The cache package from the beego project is used for redis integration, install it with **github.com/astaxie/beego/cache**

### amqp
There's some experimental code for rabbitmq integration, which requires amqp, install it with **github.com/streadway/amqp**

### aws access credential
Since the server needs to access certain aws services, a valid credential needs to be provided.

The easiest and probably the safest way is to provide the information through environment variables:

AWS_ACCESS_KEY_ID: the aws access key id

AWS_SECRET_KEY: the aws secret key

Installation
-----------------
There're 2 ways to install the server:

### Source Code
The source code for the project can be obtained vai **go get bitbucket.org/directivegames/simple-server**.

Once finished, go to the root directory of the project and run **go install**.

This will build the application and copy the executable to the binary directory of your "GOPATH", from where you can launch it directly.

### Docker
This project maintains a docker image at docker hub, it can be pulled with **docker pull haowang1013/simple-server:<tag>**

Curently valid tags include develop and master, which mirrors the code branches.

Deployment
-----------------
The server can either be run locally or deployed to the cloud.

Since the services are completely stateless, it should make no difference in terms of functionality.

However, note that since the server uses pre-defined aws services to store application state, all the running instances will share the same application "context".

### AWS Deployment
Deploying the server to aws can be done in several ways, here's an example which uses the elastic beanstalk service:

- start by creating an application in the beanstalk console
- choose "Web Server" as the environment, "Docker" as the predefined configuration, you can use either option for the environment type