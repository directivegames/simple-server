package errors

import (
	"fmt"
)

type StrParseError struct {
	Err error
}

func (this StrParseError) Error() string {
	return fmt.Sprintf("string parse error: %s", this.Err.Error())
}
