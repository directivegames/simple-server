package rabbitmq

import (
	"bitbucket.org/directivegames/simple-server/aws/dynamodb"
	"bitbucket.org/directivegames/simple-server/util"
	"fmt"
	log "github.com/cihub/seelog"
	"github.com/streadway/amqp"
	"runtime"
)

type rabbitMQContext struct {
	conn       *amqp.Connection
	channel    *amqp.Channel
	debugQueue amqp.Queue
}

var context rabbitMQContext = rabbitMQContext{}

func destroyContext(context *rabbitMQContext) {
	if context.channel != nil {
		context.channel.Close()
		context.channel = nil
	}

	if context.conn != nil {
		context.conn.Close()
		context.conn = nil
	}
}

func init() {
	runtime.SetFinalizer(&context, destroyContext)
	context.connect()
}

func (this *rabbitMQContext) connect() {
	var err error
	for {
		if address, err := dynamodb.KvTable_GetString("RabbitMQAddress"); err == nil {
			this.conn, err = amqp.Dial(address)
			if err != nil {
				log.Infof("failed to connect to rabbitmq server: %s", err.Error())
				break
			}
		} else {
			panic(fmt.Sprintf("failed to load rabbitmq address: %s", err.Error()))
		}

		this.channel, err = this.conn.Channel()
		if err != nil {
			log.Infof("failed to create channel: %s", err.Error())
			break
		}

		this.debugQueue, err = this.channel.QueueDeclare(
			"debug", // name
			false,   // durable
			false,   // delete when usused
			false,   // exclusive
			false,   // no-wait
			nil,     // arguments
		)

		if err != nil {
			log.Infof("failed to declare debug queue: %s", err.Error())
			break
		}

		ip, _ := util.GetLocalIP()
		msg := fmt.Sprintf("connected to the debug queue as server <%s>", ip)
		err = this.channel.Publish(
			"",                   // exchange
			this.debugQueue.Name, // routing key
			false,                // mandatory
			false,                // immediate
			amqp.Publishing{
				ContentType: "text/plain",
				Body:        []byte(msg),
			})

		log.Info("rabbitmq context initialized")
		break
	}
}
