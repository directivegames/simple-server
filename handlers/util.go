package handlers

import (
	"bitbucket.org/directivegames/simple-server/auth/jwt"
	"bitbucket.org/directivegames/simple-server/constant"
	"fmt"
	"github.com/gin-gonic/gin"
	"strconv"
)

func getPlayerID(c *gin.Context) (int64, error) {
	token, err := jwt.ParseToken(c.Request)
	if err == nil {
		temp, ok := token.Claims[constant.NAME_PLAYERID]
		if !ok {
			return -1, fmt.Errorf("'%s' is missing from the token", constant.NAME_PLAYERID)
		}
		playerIDStr, ok := temp.(string)
		if !ok {
			return -1, fmt.Errorf("'%s' field in the token is not of expected type", constant.NAME_PLAYERID)
		}
		playerID, err := strconv.ParseInt(playerIDStr, 10, 64)
		if err != nil {
			return -1, fmt.Errorf("failed to parse playerID: %s", err.Error())
		}
		return playerID, nil

	} else {
		return -1, err
	}
}

func verifyPlayerID(c *gin.Context) (int64, error) {
	tokenPlayerID, err := getPlayerID(c)
	if err != nil {
		return -1, err
	}

	playerID, err := strconv.ParseInt(c.Params.ByName("playerID"), 10, 64)
	if err != nil {
		return -1, fmt.Errorf("playerID is invalid")
	}

	if playerID != tokenPlayerID {
		return -1, fmt.Errorf("playerID doesn't match with the token")
	}

	return playerID, nil
}
