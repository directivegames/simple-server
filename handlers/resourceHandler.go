package handlers

import (
	"bitbucket.org/directivegames/simple-server/resources"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func init() {
	Router.GET("/players/:playerID/resources", getResources)
}

func getResources(c *gin.Context) {
	playerID, err := strconv.ParseInt(c.Params.ByName("playerID"), 10, 64)
	if err == nil {
		resources, err := resources.GetStored(playerID)
		if err == nil {
			c.JSON(http.StatusOK, resources)
		} else {
			c.String(http.StatusInternalServerError, err.Error())
		}
	} else {
		c.String(http.StatusBadRequest, "playerID is invalid")
	}
}
