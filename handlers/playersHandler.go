package handlers

import (
	"bitbucket.org/directivegames/simple-server/players"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func init() {
	rootUrls["players"] = map[string]string{
		"href":   "/players",
		"format": "/players{/playerID}",
	}
	Router.GET("/players", getPlayers)
	Router.GET("/players/:playerID", getPlayers)
	Router.PUT("/players/:playerID", updatePlayer)
}

func getPlayerInforResponse(info *players.PlayerInfo, detailedInfo bool) map[string]interface{} {
	response := map[string]interface{}{
		"playerID":   info.PlayerID,
		"playerName": info.PlayerName,
		"href":       fmt.Sprintf("/players/%d", info.PlayerID),
	}

	if detailedInfo {
		response["resources"] = map[string]string{
			"href": fmt.Sprintf("/players/%d/resources", info.PlayerID),
		}
		response["items"] = map[string]string{
			"href": fmt.Sprintf("/players/%d/items", info.PlayerID),
		}
	}
	return response
}

func getPlayers(c *gin.Context) {
	playerIDStr := c.Params.ByName("playerID")
	if len(playerIDStr) == 0 {
		players, err := players.ListPlayers(100)
		if err == nil {
			data := make([]map[string]interface{}, 0)
			for _, info := range players {
				data = append(data, getPlayerInforResponse(&info, false))
			}
			c.JSON(http.StatusOK, data)
		} else {
			c.String(http.StatusBadRequest, err.Error())
		}
	} else {
		playerID, err := strconv.ParseInt(playerIDStr, 10, 64)
		if err == nil {
			getPlayer(c, playerID)
		} else {
			c.String(http.StatusBadRequest, "playerID is invalid")
		}
	}
}

func getPlayer(c *gin.Context, playerID int64) {
	info, err := players.GetPlayerInfo(playerID, false)
	if err == nil {
		c.JSON(http.StatusOK, getPlayerInforResponse(info, true))
	} else {
		c.String(http.StatusBadRequest, err.Error())
	}
}

func updatePlayer(c *gin.Context) {
	playerID, err := verifyPlayerID(c)
	if err != nil {
		c.String(http.StatusUnauthorized, "You're not authorized to update other player's info")
		return
	}

	info := new(players.PlayerInfo)
	if c.Bind(info) {
		err := players.UpdatePlayerInfo(info)
		if err == nil {
			getPlayer(c, playerID)
		} else {
			c.String(http.StatusInternalServerError, err.Error())
		}
	} else {
		c.String(http.StatusBadRequest, "Failed to construct player info from the request")
	}
}
