package handlers

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

var rootUrls = map[string]map[string]string{}
var Router = gin.Default()

func init() {
	Router.GET("/", rootHandler)
}

func rootHandler(c *gin.Context) {
	c.JSON(http.StatusOK, rootUrls)
}
