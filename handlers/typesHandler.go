package handlers

import (
	"bitbucket.org/directivegames/simple-server/static"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func init() {
	rootUrls["types"] = map[string]string{
		"href":   "/types",
		"format": "/types{/typeID}",
	}
	Router.GET("/types", showTypes)
	Router.GET("/types/:typeID", showType)
}

func showTypes(c *gin.Context) {
	types := make(map[string]interface{})
	typeIDs := static.GetTypeIDs()
	for _, typeID := range typeIDs {
		raw := static.GetRawData(typeID)
		elem := make(map[string]interface{})
		elem["href"] = fmt.Sprintf("/types/%d", typeID)
		elem["typeName"] = raw.(map[string]interface{})["typeName"]
		types[strconv.FormatInt(typeID, 10)] = elem
	}
	c.JSON(http.StatusOK, types)
}

func showType(c *gin.Context) {
	typeID, err := strconv.ParseInt(c.Params.ByName("typeID"), 10, 64)
	if err != nil {
		c.String(http.StatusBadRequest, "invalid typeID")
		return
	}

	raw := static.GetRawData(typeID)
	if raw != nil {
		c.JSON(http.StatusOK, raw)
	} else {
		c.String(http.StatusBadRequest, "type is not found")
	}
}
