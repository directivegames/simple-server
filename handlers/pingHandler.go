package handlers

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func init() {
	rootUrls["ping"] = map[string]string{
		"href":   "/ping",
		"format": "/ping",
	}
	Router.GET("/ping", pingHandler)
}

// the ping handler
func pingHandler(c *gin.Context) {
	data := map[string]string{"response": "pong"}
	c.JSON(http.StatusOK, data)
}
