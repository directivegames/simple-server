package handlers

import (
	"bitbucket.org/directivegames/simple-server/auth"
	"bitbucket.org/directivegames/simple-server/auth/jwt"
	"bitbucket.org/directivegames/simple-server/config"
	"bitbucket.org/directivegames/simple-server/constant"
	"bitbucket.org/directivegames/simple-server/players"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func init() {
	rootUrls["authentication"] = map[string]string{
		"href":   "/auth",
		"format": "/auth/{deviceID}",
	}
	Router.GET("/auth/:deviceID", getToken)
}

func getToken(c *gin.Context) {
	deviceID := c.Params.ByName("deviceID")
	if len(deviceID) == 0 {
		c.String(http.StatusBadRequest, "missing 'deviceID' param")
		return
	}

	playerID, err := auth.GetPlayerID(deviceID)
	if err == nil {
		playerInfo, err := players.ConditionalInitPlayer(playerID)
		if err == nil {
			claims := make(map[string]interface{})
			claims["sub"] = constant.NAME_PLAYERID
			claims[constant.NAME_PLAYERID] = strconv.FormatInt(playerInfo.PlayerID, 10)
			token, err := jwt.GetToken(&claims, config.JwtExpireTime)
			if err == nil {
				data := map[string]string{"token": token}
				c.JSON(http.StatusOK, data)
			} else {
				c.String(http.StatusInternalServerError, err.Error())
			}
		} else {
			c.String(http.StatusInternalServerError, err.Error())
		}

	} else {
		c.String(http.StatusInternalServerError, err.Error())
	}
}
