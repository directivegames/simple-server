package handlers

import (
	"bitbucket.org/directivegames/simple-server/id"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

const (
	tableName = "IssuedIDs"
	keyName   = "Name"
	valueName = "LastIssued"
)

func init() {
	rootUrls["uid"] = map[string]string{
		"href":   "/uids",
		"format": "/uids{/name}",
	}
	Router.GET("/uids", listAllIDs)
	Router.POST("/uids/:name", modifyID)
}

func listIDs() (map[string]string, error) {
	// list all the ids
	ids, err := id.ListIDs()
	if err != nil {
		return nil, err
	}
	converted := make(map[string]string)
	for k, v := range ids {
		converted[k] = strconv.FormatInt(v, 10)
	}
	return converted, nil
}

func listAllIDs(c *gin.Context) {
	collection, err := listIDs()
	if err == nil {
		c.JSON(http.StatusOK, collection)
	} else {
		c.String(http.StatusInternalServerError, err.Error())
	}
}

func modifyID(c *gin.Context) {
	name := c.Params.ByName("name")
	newID, err := id.GenerateNewID(name)
	if err == nil {
		data := map[string]int64{name: newID}
		c.JSON(http.StatusOK, data)
	} else {
		c.String(http.StatusInternalServerError, err.Error())
	}
}
