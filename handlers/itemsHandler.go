package handlers

import (
	"bitbucket.org/directivegames/simple-server/resources"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func init() {
	Router.GET("/players/:playerID/items", getItems)
	Router.GET("/players/:playerID/items/:itemID", getItem)
	Router.PUT("/players/:playerID/items/:itemID", updateItem)
	Router.POST("/players/:playerID/items", createItem)
}

func getItemResponse(playerID int64, item *resources.PlayerItem) map[string]interface{} {
	response := map[string]interface{}{
		"itemID": item.ItemID,
		"typeID": item.TypeID,
		"level":  item.Level,
		"posX":   item.PosX,
		"posY":   item.PosY,
		"posZ":   item.PosZ,
		"href":   fmt.Sprintf("/players/%d/items/%d", playerID, item.ItemID),
	}
	return response
}

func getItems(c *gin.Context) {
	playerID, err := strconv.ParseInt(c.Params.ByName("playerID"), 10, 64)
	if err == nil {
		items, err := resources.GetPlayerItems(playerID)
		if err == nil {
			data := make([]map[string]interface{}, 0)
			for _, elem := range items {
				data = append(data, getItemResponse(playerID, &elem))
			}
			c.JSON(http.StatusOK, data)
		} else {
			c.String(http.StatusInternalServerError, err.Error())
		}
	} else {
		c.String(http.StatusBadRequest, "playerID is invalid")
	}
}

func getItem(c *gin.Context) {
	playerID, err := strconv.ParseInt(c.Params.ByName("playerID"), 10, 64)
	if err != nil {
		c.String(http.StatusBadRequest, "playerID is invalid")
		return
	}
	itemID, err := strconv.ParseInt(c.Params.ByName("itemID"), 10, 64)
	if err != nil {
		c.String(http.StatusBadRequest, "itemID is invalid")
		return
	}
	item, err := resources.GetPlayerItem(playerID, itemID)
	if err == nil {
		c.JSON(http.StatusOK, getItemResponse(playerID, item))
	} else {
		c.String(http.StatusInternalServerError, err.Error())
	}
}

func createItem(c *gin.Context) {
	playerID, err := verifyPlayerID(c)
	if err != nil {
		c.String(http.StatusUnauthorized, err.Error())
		return
	}

	request := new(resources.PlayerItem)
	if c.Bind(request) {
		newItem, err := resources.CreateItem(playerID, request)
		if err != nil {
			c.String(http.StatusBadRequest, fmt.Sprintf("failed to create item: %s", err.Error()))
		} else {
			c.JSON(http.StatusOK, getItemResponse(playerID, newItem))
		}
	} else {
		c.String(http.StatusBadRequest, "failed to construct player item from the request")
	}
}

func updateItem(c *gin.Context) {
	playerID, err := verifyPlayerID(c)
	if err != nil {
		c.String(http.StatusUnauthorized, err.Error())
		return
	}

	request := new(resources.PlayerItem)
	if c.Bind(request) {
		newItem, err := resources.UpdateItem(playerID, request)
		if err != nil {
			c.String(http.StatusBadRequest, fmt.Sprintf("failed to update item: %s", err.Error()))
		} else {
			c.JSON(http.StatusOK, getItemResponse(playerID, newItem))
		}
	} else {
		c.String(http.StatusBadRequest, "failed to construct player item from the request")
	}
}
