package players

import (
	"bitbucket.org/directivegames/simple-server/resources"
	"bitbucket.org/directivegames/simple-server/static"
	"fmt"
	log "github.com/cihub/seelog"
)

func ConditionalInitPlayer(playerID int64) (*PlayerInfo, error) {
	playerInfo, err := GetPlayerInfo(playerID, true)
	if err == nil {
		if playerInfo == nil {
			log.Infof("initializing player %d", playerID)

			// delete all existing items in the db so the player is given a fresh start
			err = resources.DeleteItemsInDB(playerID)
			if err != nil {
				return nil, err
			}

			// create the items
			err = resources.CreateDefaultItems(playerID, static.NewPlayerConfigType.DefaultBuildings)
			if err != nil {
				return nil, err
			}

			// create the default resources
			defaultResources := resources.ResourceCollection{}
			for _, elem := range static.NewPlayerConfigType.DefaultResources {
				defaultResources.AddResource(elem.ResourceType, elem.Amount, true)
			}

			_, err = resources.AddResources(playerID, defaultResources)
			if err != nil {
				return nil, err
			}

			// create the player itself
			playerInfo = &PlayerInfo{
				PlayerID:   playerID,
				PlayerName: fmt.Sprintf("Player %d", playerID),
			}

			err = UpdatePlayerInfo(playerInfo)

			if err != nil {
				return nil, err
			}

			log.Infof("player %d initialized", playerID)
			playerItems, _ := resources.GetPlayerItems(playerID)
			for index, elem := range playerItems {
				log.Debugf("item %d: %s", index, elem)
			}

			stored, capacity, _ := resources.GetStoredAndCapacity(playerID)
			log.Debugf("stored resource: %s", stored)
			log.Debugf("resource capacity: %s", capacity)
			return playerInfo, nil
		} else {
			return playerInfo, nil
		}
	} else {
		return nil, err
	}
}
