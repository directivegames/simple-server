package players

import (
	"bitbucket.org/directivegames/simple-server/aws/dynamodb"
	"github.com/smugmug/godynamo/types/item"
	"strconv"
)

type PlayerInfo struct {
	PlayerID   int64  `json:"playerID" binding:"required"`
	PlayerName string `json:"playerName" binding:"required"`
}

func (this *PlayerInfo) FromDBItem(row item.Item) error {
	playerID, err := strconv.ParseInt(row["PlayerID"].N, 10, 64)
	if err == nil {
		this.PlayerID = playerID
		this.PlayerName = row["PlayerName"].S
		return nil
	} else {
		return err
	}
}

func (this *PlayerInfo) ToDBItem() item.Item {
	row := item.NewItem()
	row[playersInfoTable.KeyName] = dynamodb.GetNumberAttribute(this.PlayerID)
	row["PlayerName"] = dynamodb.GetStringAttribute(this.PlayerName)
	return row
}
