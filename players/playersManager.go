package players

import (
	"bitbucket.org/directivegames/simple-server/aws/dynamodb"
	"fmt"
)

var (
	playersInfoTable = dynamodb.Table{
		TableName: "PlayersInfo",
		KeyName:   "PlayerID",
	}
)

func ListPlayers(limit uint64) ([]PlayerInfo, error) {
	items, err := playersInfoTable.Scan(limit)
	if err != nil {
		return nil, err
	}

	players := make([]PlayerInfo, 0)
	for _, elem := range items {
		info := PlayerInfo{}
		err := info.FromDBItem(elem)
		if err == nil {
			players = append(players, info)
		} else {
			return nil, err
		}
	}
	return players, nil
}

func GetPlayerInfo(playerID int64, allowNull bool) (*PlayerInfo, error) {
	keyValue := dynamodb.GetNumberAttribute(playerID)
	row, err := playersInfoTable.GetItem(keyValue)
	if err == nil {
		if row == nil {
			if allowNull {
				return nil, nil
			} else {
				return nil, fmt.Errorf("Player doesn't exist under id %s", playerID)
			}
		} else {
			info := PlayerInfo{}
			err := info.FromDBItem(row)
			if err == nil {
				return &info, nil
			} else {
				return nil, err
			}
		}
	} else {
		return nil, err
	}
}

func UpdatePlayerInfo(info *PlayerInfo) error {
	row := info.ToDBItem()
	err := playersInfoTable.PutItem(row)
	return err
}
