package cache

import (
	"bitbucket.org/directivegames/simple-server/aws/dynamodb"
	"fmt"
	"github.com/astaxie/beego/cache"
	_ "github.com/astaxie/beego/cache/redis"
	log "github.com/cihub/seelog"
	"log"
	"strconv"
)

var Instance cache.Cache

func init() {
	if address, err := dynamodb.KvTable_GetString("RedisAddress"); err == nil {
		redisConfig := fmt.Sprintf(`{"conn":"%s"}`, address)
		var err error
		Instance, err = cache.NewCache("redis", redisConfig)

		if err != nil {
			msg := fmt.Sprintf("failed to init redis cache: %s", err)
			log.Fatal(msg)
		}

		log.Infof("redis cache initialized with address: %s", address)
	} else {
		panic(fmt.Sprintf("failed to load redis address: %s", err.Error()))
	}
}

func AsInt(val interface{}) (int, error) {
	buff, ok := val.([]uint8)
	if ok {
		temp := string(buff)
		return strconv.Atoi(temp)
	} else {
		return -1, fmt.Errorf("failed to convert redis response to byte buffer: %s", val)
	}
}

func AsString(val interface{}) (string, error) {
	buff, ok := val.([]uint8)
	if ok {
		return string(buff), nil
	} else {
		return "", fmt.Errorf("failed to convert redis response to byte buffer: %s", val)
	}
}
