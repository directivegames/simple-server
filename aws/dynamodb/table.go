package dynamodb

import (
	"encoding/json"
	"fmt"
	log "github.com/cihub/seelog"
	"github.com/smugmug/godynamo/endpoint"
	bg "github.com/smugmug/godynamo/endpoints/batch_get_item"
	bw "github.com/smugmug/godynamo/endpoints/batch_write_item"
	"github.com/smugmug/godynamo/endpoints/get_item"
	"github.com/smugmug/godynamo/endpoints/put_item"
	"github.com/smugmug/godynamo/endpoints/query"
	"github.com/smugmug/godynamo/endpoints/scan"
	"github.com/smugmug/godynamo/endpoints/update_item"
	"github.com/smugmug/godynamo/types/attributevalue"
	"github.com/smugmug/godynamo/types/condition"
	"github.com/smugmug/godynamo/types/item"
	"strconv"
)

type Table struct {
	TableName    string
	KeyName      string
	RangeKeyName string
}

const (
	emptyResponse = "{}"
)

func (this Table) GetItem(keyValue *attributevalue.AttributeValue) (item.Item, error) {
	var op = get_item.NewGetItem()
	op.ConsistentRead = true
	op.Key[this.KeyName] = keyValue
	op.TableName = this.TableName
	body, _, err := op.EndpointReq()
	if err != nil {
		log.Errorf("GetItem failed with error: %s, table=%s, key=%s", err.Error(), this.TableName, keyValue)
		return nil, err
	}

	if string(body) == emptyResponse {
		return nil, nil
	} else {
		response := get_item.NewResponse()
		err = json.Unmarshal(body, &response)
		if err != nil {
			log.Errorf("GetItem: json unmarshal error: %s, table=%s, key=%s, body=%s", err.Error(), this.TableName, keyValue, body)
			return nil, err
		}
		return response.Item, nil
	}
}

func (this Table) PutItem(item item.Item) error {
	op := put_item.NewPutItem()
	op.TableName = this.TableName
	op.Item = item
	_, _, err := op.EndpointReq()
	if err != nil {
		log.Errorf("PutItem failed with error: %s, table=%s, item=%s", err.Error(), this.TableName, item)
	}
	return err
}

func (this Table) BatchGetItems(keys []item.Item) ([]item.Item, error) {
	op := bg.NewBatchGetItem()
	requests := bg.NewRequestInstance()
	requests.Keys = keys
	op.RequestItems[this.TableName] = requests
	body, _, err := op.EndpointReq()
	if err != nil {
		log.Errorf("BatchGetItems failed with error: %s, table=%s", err.Error(), this.TableName)
		return nil, err
	}

	response := bg.NewResponse()
	err = json.Unmarshal(body, &response)
	if err != nil {
		log.Errorf("BatchGetItems: json unmarshal error: %s, table=%s", err.Error(), this.TableName)
		return nil, err
	}

	return response.Responses[this.TableName], nil
}

func (this Table) Scan(limit uint64) ([]item.Item, error) {
	s := scan.NewScan()
	s.TableName = this.TableName
	s.Limit = limit
	body, _, err := s.EndpointReq()
	if err != nil {
		log.Errorf("Scan failed with error: %s, table=%s, limit=%d", err.Error(), this.TableName, limit)
		return nil, err
	}

	response := scan.NewResponse()
	err = json.Unmarshal(body, &response)
	if err != nil {
		log.Errorf("Scan: json unmarshal error: %s, table=%s, body=%s", err.Error(), this.TableName, body)
		return nil, err
	}

	return response.Items, nil
}

func (this Table) DeleteItemsWithHashKey(keys []*attributevalue.AttributeValue) error {
	if len(keys) == 0 {
		return nil
	}

	op := bw.NewBatchWriteItem()
	requests := make([]bw.RequestInstance, 0)

	for _, elem := range keys {
		var request bw.DeleteRequest
		request.Key = item.NewItem()
		request.Key[this.KeyName] = elem
		requests = append(requests, bw.RequestInstance{DeleteRequest: &request})
	}
	op.RequestItems[this.TableName] = requests
	_, _, err := op.EndpointReq()
	if err != nil {
		log.Errorf("DeleteItemsWithHashKey failed with error: %s, table=%s", err.Error(), this.TableName)
	}
	return err
}

func (this Table) DeleteItemsWithHashAndRangeKey(hkeys, rkeys []*attributevalue.AttributeValue) error {
	if len(hkeys) != len(rkeys) {
		return fmt.Errorf("the length of the hash keys and range keys are not the same")
	}

	if len(hkeys) == 0 {
		return nil
	}

	op := bw.NewBatchWriteItem()
	requests := make([]bw.RequestInstance, 0)

	for index, elem := range hkeys {
		var request bw.DeleteRequest
		request.Key = item.NewItem()
		request.Key[this.KeyName] = elem
		request.Key[this.RangeKeyName] = rkeys[index]
		requests = append(requests, bw.RequestInstance{DeleteRequest: &request})
	}
	op.RequestItems[this.TableName] = requests
	_, _, err := op.EndpointReq()
	if err != nil {
		log.Errorf("DeleteItemsWithHashAndRangeKey failed with error: %s, table=%s", err.Error(), this.TableName)
	}
	return err
}

func (this Table) UpdateItems(items []item.Item) error {
	if len(items) == 0 {
		return nil
	}
	op := bw.NewBatchWriteItem()
	requests := make([]bw.RequestInstance, len(items))
	for index, elem := range items {
		var request bw.PutRequest
		request.Item = elem
		requests[index] = bw.RequestInstance{PutRequest: &request}
	}
	op.RequestItems[this.TableName] = requests
	_, _, err := op.EndpointReq()
	if err != nil {
		log.Errorf("UpdateItems failed with error: %s, table=%s", err.Error(), this.TableName)
	}
	return err
}

func (this Table) QueryWithHashKey(keyValue *attributevalue.AttributeValue) ([]item.Item, error) {
	q := query.NewQuery()
	q.TableName = this.TableName
	q.Select = endpoint.SELECT_ALL
	kc := condition.NewCondition()
	kc.AttributeValueList = make([]*attributevalue.AttributeValue, 1)
	kc.AttributeValueList[0] = keyValue
	kc.ComparisonOperator = query.OP_EQ
	q.KeyConditions[this.KeyName] = kc
	body, _, err := q.EndpointReq()
	if err != nil {
		log.Errorf("QueryWithHashKey failed with error: %s, table=%s, key=%s", err.Error(), this.TableName, keyValue)
		return nil, err
	}

	response := query.NewResponse()
	err = json.Unmarshal(body, &response)
	if err != nil {
		log.Errorf("QueryWithHashKey: json unmarshal error: %s, table=%s, body=%s", err.Error(), this.TableName, body)
		return nil, err
	}

	return response.Items, nil

}

func (this Table) QueryWithHashAndRangeKey(hashKeyValue, rangeKeyValue *attributevalue.AttributeValue) ([]item.Item, error) {
	q := query.NewQuery()
	q.TableName = this.TableName
	q.Select = endpoint.SELECT_ALL
	kc := condition.NewCondition()
	kc.AttributeValueList = make([]*attributevalue.AttributeValue, 1)
	kc.AttributeValueList[0] = hashKeyValue
	kc.ComparisonOperator = query.OP_EQ
	q.KeyConditions[this.KeyName] = kc

	rkc := condition.NewCondition()
	rkc.AttributeValueList = make([]*attributevalue.AttributeValue, 1)
	rkc.AttributeValueList[0] = rangeKeyValue
	rkc.ComparisonOperator = query.OP_EQ
	q.KeyConditions[this.RangeKeyName] = rkc

	body, _, err := q.EndpointReq()
	if err != nil {
		log.Errorf("QueryWithHashAndRangeKey failed with error: %s, table=%s, hkey=%s, rkey=%s", err.Error(), this.TableName, hashKeyValue, rangeKeyValue)
		return nil, err
	}

	response := query.NewResponse()
	err = json.Unmarshal(body, &response)
	if err != nil {
		log.Errorf("QueryWithHashAndRangeKey: json unmarshal error: %s, table=%s, body=%s", err.Error(), this.TableName, body)
		return nil, err
	}

	return response.Items, nil

}

func (this Table) AtomicAdd(keyValue *attributevalue.AttributeValue, valueName string, delta int64) (int64, error) {
	op := update_item.NewUpdateItem()
	op.TableName = this.TableName
	op.Key[this.KeyName] = keyValue
	op.UpdateExpression = fmt.Sprintf("set %s = %s + :num", valueName, valueName)
	op.ExpressionAttributeValues[":num"] = GetNumberAttribute(delta)
	op.ReturnValues = "UPDATED_NEW"
	body, _, err := op.EndpointReq()
	if err != nil {
		log.Errorf("AtomicAdd failed with error: %s, table=%s, key=%s, valueName=%s", err.Error(), this.TableName, keyValue, valueName)
		return 0, err
	}

	response := update_item.NewResponse()
	err = json.Unmarshal([]byte(body), response)
	if err != nil {
		log.Errorf("AtomicAdd: json unmarshal error: %s, table=%s, key=%s, body=%s", err.Error(), this.TableName, keyValue, body)
		return 0, err
	}

	newValue, err := strconv.ParseInt(response.Attributes[valueName].N, 10, 64)
	return newValue, err

}
