package dynamodb

var (
	kvTable = Table{TableName: "KeyValue", KeyName: "Key"}
)

func KvTable_GetString(key string) (string, error) {
	item, err := kvTable.GetItem(GetStringAttribute(key))
	if err != nil {
		return "", err
	}

	value, err := GetStringValue(item, "Value", false)
	if err != nil {
		return "", err
	}
	return *value, nil
}
