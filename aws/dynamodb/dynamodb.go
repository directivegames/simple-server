package dynamodb

import (
	_ "bitbucket.org/directivegames/simple-server/aws/config"
	"fmt"
	log "github.com/cihub/seelog"
	"github.com/smugmug/godynamo/types/attributevalue"
	"github.com/smugmug/godynamo/types/item"
	"strconv"
)

func GetNumberAttribute(number int64) *attributevalue.AttributeValue {
	return &attributevalue.AttributeValue{N: strconv.FormatInt(number, 10)}
}

func GetStringAttribute(text string) *attributevalue.AttributeValue {
	return &attributevalue.AttributeValue{S: text}
}

func GetNumberValue(row item.Item, fieldName string, allowNull bool) (*int64, error) {
	elem, ok := row[fieldName]
	if !ok {
		if allowNull {
			return nil, nil
		} else {
			err := fmt.Errorf("field doesn't exist: %s", fieldName)
			log.Error(err.Error())
			return nil, err
		}
	}

	parsed, err := strconv.ParseInt(elem.N, 10, 64)
	if err != nil {
		log.Errorf("failed to parse int: %s", err.Error())
		return nil, err
	}

	return &parsed, nil
}

func GetStringValue(row item.Item, fieldName string, allowNull bool) (*string, error) {
	elem, ok := row[fieldName]
	if !ok {
		if allowNull {
			return nil, nil
		} else {
			err := fmt.Errorf("field doesn't exist: %s", fieldName)
			log.Error(err.Error())
			return nil, err
		}
	}

	return &elem.S, nil
}
