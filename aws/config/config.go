package config

import (
	"bitbucket.org/directivegames/simple-server/event"
	log "github.com/cihub/seelog"
	"github.com/smugmug/godynamo/conf"
	"github.com/smugmug/godynamo/conf_file"
	"os"
)

func init() {
	conf_file.Read()
	conf.Vals.ConfLock.RLock()
	if conf.Vals.Initialized == false {
		panic("the conf.Vals global conf struct has not been initialized")
	}

	if len(conf.Vals.Auth.AccessKey) == 0 {
		log.Info("aws access id is not specified in the config file")
		conf.Vals.Auth.AccessKey = os.Getenv("AWS_ACCESS_KEY_ID")
		if len(conf.Vals.Auth.AccessKey) > 0 {
			log.Info("got aws access id from env variable")
		}
	}

	if len(conf.Vals.Auth.Secret) == 0 {
		log.Info("aws secret key is not specified in the config file")
		conf.Vals.Auth.Secret = os.Getenv("AWS_SECRET_ACCESS_KEY")
		if len(conf.Vals.Auth.Secret) == 0 {
			conf.Vals.Auth.Secret = os.Getenv("AWS_SECRET_KEY")
		}

		if len(conf.Vals.Auth.Secret) > 0 {
			log.Info("got aws secret key from env variable")
		}
	}

	if len(conf.Vals.Auth.AccessKey) == 0 || len(conf.Vals.Auth.Secret) == 0 {
		panic("failed to init aws credentials from the config file or env variables")
	}

	conf.Vals.ConfLock.RUnlock()

	event.TriggerEvent(event.EVENT_AwsConfigInitialized, nil)
}
