package id

import (
	"bitbucket.org/directivegames/simple-server/aws/dynamodb"
	"github.com/smugmug/godynamo/types/item"
)

const (
	valueName = "LastIssued"
)

var idTable = dynamodb.Table{
	TableName: "IssuedIDs",
	KeyName:   "Name",
}

func GenerateNewID(idName string) (int64, error) {
	keyValue := dynamodb.GetStringAttribute(idName)
	row, err := idTable.GetItem(keyValue)
	if err == nil {
		if row == nil {
			// new id
			row = item.NewItem()
			row[idTable.KeyName] = keyValue
			row[valueName] = dynamodb.GetNumberAttribute(1)
			err := idTable.PutItem(row)
			if err == nil {
				return 1, nil
			} else {
				return 0, err
			}
		} else {
			newID, err := idTable.AtomicAdd(keyValue, valueName, 1)
			return newID, err
		}
	} else {
		return 0, err
	}
}

func ListIDs() (map[string]int64, error) {
	items, err := idTable.Scan(100)
	if err != nil {
		return nil, err
	}

	ids := make(map[string]int64)
	for _, elem := range items {
		name, err := dynamodb.GetStringValue(elem, idTable.KeyName, false)
		if err == nil {
			value, err := dynamodb.GetNumberValue(elem, valueName, false)
			if err == nil {
				ids[*name] = *value
			}
		}
	}

	return ids, nil
}
