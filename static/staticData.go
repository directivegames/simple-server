package static

import (
	"bitbucket.org/directivegames/simple-server/constant"
	"bitbucket.org/directivegames/simple-server/schemas"
	"bitbucket.org/directivegames/simple-server/util"
	"encoding/json"
	"fmt"
	log "github.com/cihub/seelog"
	"strconv"
)

var rootData = make(map[int64]interface{})
var NewPlayerConfigType = new(schemas.NewPlayerConfig)

const (
	staticDataUrl = "http://the-machines.s3.amazonaws.com/static-data/dev/develop/types/types.json"
)

func init() {
	buffer, err := util.DownloadFromUrl(staticDataUrl)
	if err == nil {
		log.Infof("static data downlaoded with %d bytes", len(buffer))
		temp := make(map[string]interface{})
		err := json.Unmarshal(buffer, &temp)
		if err != nil {
			panic(fmt.Sprintf("parsed with error: %s", err.Error()))
		}
		for k, v := range temp {
			typeID, err := strconv.ParseInt(k, 10, 64)
			if err != nil {
				log.Errorf("failed to parse typeID: %s, err=%s", k, err.Error())
				continue
			}
			rootData[typeID] = v
		}
	} else {
		panic(fmt.Sprintf("failed to download static data: %s", err.Error()))
	}

	err = PopulateType(constant.NEW_PLAYER_CONFIG_TYPEID, NewPlayerConfigType)
	if err == nil {
		log.Info("new player config loaded")
	} else {
		panic(fmt.Sprintf("failed to populate new play config type: %s", err.Error()))
	}
}

func PopulateType(typeID int64, obj interface{}) error {
	data, ok := rootData[typeID]
	if ok {
		encoded, err := json.Marshal(data)
		if err == nil {
			err := json.Unmarshal(encoded, obj)
			if err != nil {
				return fmt.Errorf("failed to decode the data with error: %s", err.Error())
			} else {
				return nil
			}
		} else {
			return fmt.Errorf("failed to encode the loaded data with error: %s", err.Error())
		}
	} else {
		return fmt.Errorf("type doesn't exist")
	}
}

func GetTypeIDs() []int64 {
	typeIDs := make([]int64, 0)
	for k, _ := range rootData {
		typeIDs = append(typeIDs, k)
	}
	return typeIDs
}

func GetRawData(typeID int64) interface{} {
	return rootData[typeID]
}
