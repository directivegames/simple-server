package jwt

import (
	jwt "github.com/dgrijalva/jwt-go"
	"net/http"
	"time"
)

var (
	privateKey []byte
	publicKey  []byte
)

func GetToken(claims *map[string]interface{}, ttlSec int) (string, error) {
	// Create the token
	token := jwt.New(jwt.GetSigningMethod("RS256"))
	token.Claims = *claims
	// Set some claims
	token.Claims["iss"] = "simple-server"
	token.Claims["iat"] = time.Now().Unix()
	if ttlSec > 0 {
		token.Claims["exp"] = time.Now().Add(time.Second * time.Duration(ttlSec)).Unix()
	}
	// Sign and get the complete encoded token as a string
	tokenString, err := token.SignedString(privateKey)
	if err == nil {
		return tokenString, nil
	} else {
		return "", err
	}
}

func ParseToken(req *http.Request) (*jwt.Token, error) {
	token, err := jwt.ParseFromRequest(req, func(token *jwt.Token) (interface{}, error) {
		return publicKey, nil
	})

	return token, err
}
