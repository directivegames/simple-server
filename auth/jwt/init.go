package jwt

import (
	"bitbucket.org/directivegames/simple-server/aws/dynamodb"
	"fmt"
	log "github.com/cihub/seelog"
)

const (
	privateKeyName = "RSA_PRIVATE_KEY"
	publicKeyName  = "RSA_PUBLIC_KEY"
)

func init() {
	if value, err := dynamodb.KvTable_GetString(privateKeyName); err == nil {
		privateKey = []byte(value)
	} else {
		panic(fmt.Sprintf("failed to load private key: %s", err.Error()))
	}

	if value, err := dynamodb.KvTable_GetString(publicKeyName); err == nil {
		publicKey = []byte(value)
	} else {
		panic(fmt.Sprintf("failed to load public key: %s", err.Error()))
	}

	log.Info("rsa public and private keys initialized")
}
