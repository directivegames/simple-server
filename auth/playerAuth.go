package auth

import (
	"bitbucket.org/directivegames/simple-server/aws/dynamodb"
	"bitbucket.org/directivegames/simple-server/id"
	"fmt"
	"github.com/smugmug/godynamo/types/item"
)

var (
	deviceTable = dynamodb.Table{
		TableName: "PlayerDevices",
		KeyName:   "DeviceID",
	}
	valueName = "PlayerID"
)

func GetPlayerID(deviceID string) (int64, error) {
	if len(deviceID) == 0 {
		return -1, fmt.Errorf("invalid device id")
	}

	var playerID int64

	keyValue := dynamodb.GetStringAttribute(deviceID)
	row, err := deviceTable.GetItem(keyValue)
	if err != nil {
		return -1, err
	}

	if row == nil {
		// the device is not registered before
		playerID, err = id.GenerateNewID("PlayerID")
		if err != nil {
			return -1, err
		}

		row = item.NewItem()
		row[deviceTable.KeyName] = keyValue
		row[valueName] = dynamodb.GetNumberAttribute(playerID)
		err = deviceTable.PutItem(row)
		if err != nil {
			return -1, err
		}
		return playerID, nil

	} else {
		// existing item
		playerID, err := dynamodb.GetNumberValue(row, valueName, false)
		if err != nil {
			return -1, err
		}
		return *playerID, nil
	}

}
