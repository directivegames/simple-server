package main

import (
	_ "bitbucket.org/directivegames/simple-server/aws/config"
	"bitbucket.org/directivegames/simple-server/config"
	"bitbucket.org/directivegames/simple-server/handlers"
	_ "bitbucket.org/directivegames/simple-server/rabbitmq"
	_ "bitbucket.org/directivegames/simple-server/schemas"
	_ "bitbucket.org/directivegames/simple-server/static"
	"fmt"
	log "github.com/cihub/seelog"
	"os"
	"runtime"
)

func main() {
	if workingDir, err := os.Getwd(); err == nil {
		log.Infof("current working dir: %s", workingDir)
	}
	runtime.GOMAXPROCS(runtime.NumCPU())

	log.Infof("starting server at port: %d", config.ListenPort)
	addr := fmt.Sprintf(":%d", config.ListenPort)
	handlers.Router.Run(addr)
	log.Info("server closed")
}
