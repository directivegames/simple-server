package schemas

type BuildingSpec struct {
	TypeID       int64
	DisplayName  LocalizedString
	Description  LocalizedString
	TypeName     string
	Levels       []BuildingLevelInfo
	BuildingType string
}

type BuildingPlacement struct {
	TypeID   int64
	Location Vector
}

type LocalizedString struct {
	Text     string
	StringID int64
}

type ResourceDetail struct {
	ResourceType string
	Amount       int64
}

type Vector struct {
	Y float32
	X float32
	Z float32
}

type NewPlayerConfig struct {
	TypeID           int64
	DefaultBuildings []BuildingPlacement
	DefaultResources []ResourceDetail
	TypeName         string
}

type BuildingLevelInfo struct {
	UpgradeTime   int64
	MaxBuildings  int64
	HqLevelNeeded int64
	Storage       []ResourceDetail
	Cost          []ResourceDetail
	Health        int64
	XpGained      int64
	XpLevelNeeded int64
	Output        []ResourceDetail
}

type RootSpec struct {
	TypeID        int64
	BuildingTypes []int64
	TypeName      string
}
