package event

type EventHandler interface {
	OnActivated(param interface{})
}

var eventHandlers = make(map[string][]EventHandler)

func RegisterHandler(eventName string, handler EventHandler) {
	var handlers []EventHandler
	var ok bool
	if handlers, ok = eventHandlers[eventName]; !ok {
		handlers = make([]EventHandler, 10)
	}
	eventHandlers[eventName] = append(handlers, handler)
}

func TriggerEvent(eventName string, param interface{}) {
	if handlers, ok := eventHandlers[eventName]; ok {
		for _, hanlder := range handlers {
			if hanlder != nil {
				hanlder.OnActivated(param)
			}
		}
	}
}
