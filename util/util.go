package util

import (
	"encoding/json"
	"fmt"
	log "github.com/cihub/seelog"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"reflect"
)

func describe(address net.Addr) string {
	return fmt.Sprintf("%s (%s)", reflect.TypeOf(address), address)
}

func GetLocalIP() (string, error) {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return "", err
	}

	for _, a := range addrs {
		if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String(), nil
			}
		}
	}

	return "", fmt.Errorf("failed to get ip address")
}

func ListLocalIPs() {
	log.Info("listing local addresses:")
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		log.Info("failed to get ip address")
	}

	for index, a := range addrs {
		log.Infof("%d: %s", index, describe(a))
	}
}

func WriteJsonResponse(w http.ResponseWriter, data interface{}) error {
	bytes, err := json.MarshalIndent(data, "", " ")
	if err == nil {
		io.WriteString(w, string(bytes))
		return nil
	} else {
		return err
	}
}

func WriteDefaultStatusText(w http.ResponseWriter, code int) {
	http.Error(w, http.StatusText(code), code)
}

func PrintEnvVariables() {
	log.Info("printing environment variables:")
	for _, env := range os.Environ() {
		log.Info("  ", env)
	}
}

func DownloadFromUrl(url string) ([]byte, error) {
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	return ioutil.ReadAll(response.Body)
}

func MinInt64(a, b int64) int64 {
	if a > b {
		return b
	} else {
		return a
	}
}

func MaxInt64(a, b int64) int64 {
	if a > b {
		return a
	} else {
		return b
	}
}
