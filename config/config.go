package config

var ListenPort = 8080
var CertFilePath = ""    //"./tls/cert.pem"
var KeyFilePath = ""     //"./tls/key.pem"
var JwtExpireTime = 3600 // how much time until the issued jwt expires, in sec
